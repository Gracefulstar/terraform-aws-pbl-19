region = "eu-west-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-06b5e063b6c8a304c"

ami-bastion = "ami-0a5fd36ff1da035c8"

ami-nginx = "ami-073ac78e429819ad2"

ami-sonar = "ami-0db1d1fbfd402536b"

keypair = "doyin"

master-password = "gracefulstar"

master-username = "gracefulstar"

account_no = "987157516801"

tags = {
  Owner-Email = "gracelasupo19@gmail.com"
  Managed-By  = "Terraform"

}